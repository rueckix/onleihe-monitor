# syntax=docker/dockerfile:1

FROM alpine:3.17

RUN apk update && apk add libxslt curl bash diffutils

# Set destination for COPY
WORKDIR /app

# Download Go modules
COPY . .

ENTRYPOINT ["/bin/bash", "/app/onleihe-monitor.sh"]







